package com.seges.graphql.services;

import org.springframework.stereotype.Service;

/**
 * Simple REST GET endpoint for comparison with GraphQL
 */
@Service
public class HelloworldService {

    public String sayHello() {
        return "Hello World";
    }

}
