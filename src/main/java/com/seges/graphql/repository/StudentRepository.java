package com.seges.graphql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.seges.graphql.model.Student;

/**
 * Created by pmelus on 28.6.2018.
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
}
