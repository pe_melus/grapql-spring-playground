package com.seges.graphql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.seges.graphql.model.Book;

/**
 * Created by pmelus on 1.7.2018.
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
}
