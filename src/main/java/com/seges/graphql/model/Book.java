package com.seges.graphql.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.Data;

/**
 * Created by pmelus on 28.6.2018.
 * Using lombok DATA annotation for auto generate (default) getters and setters
 */
@Data
@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bookSeq")
    @SequenceGenerator(name = "bookSeq", sequenceName = "BOOK_SEQ", allocationSize = 1)
    private Long id;
    private String name;
    @ManyToOne
    private Student student;

    public Book(String name) {
        this.name = name;
    }

    public Book() {}

}
