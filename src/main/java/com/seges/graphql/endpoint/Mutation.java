package com.seges.graphql.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.seges.graphql.model.Book;
import com.seges.graphql.model.Student;
import com.seges.graphql.repository.BookRepository;
import com.seges.graphql.repository.StudentRepository;

/**
 * Has all modification endpoints for every entity
 * Created by pmelus on 28.6.2018.
 */
@Component
public class Mutation implements GraphQLMutationResolver {

    private final StudentRepository studentRepository;
    private final BookRepository bookRepository;

    @Autowired
    public Mutation(BookRepository bookRepository, StudentRepository studentRepository) {
        this.bookRepository = bookRepository;
        this.studentRepository = studentRepository;
    }

    public Student createStudent(String firstName, String lastName) {
        return studentRepository.save(new Student(firstName, lastName));
    }

    public Book createBook(String name) {
        return bookRepository.save(new Book(name));
    }

    public Student updateStudent(Long id, String firstName, String lastName) {
        Student student = studentRepository.findOne(id);
        student.setFirstName(firstName);
        student.setLastName(lastName);
        return studentRepository.save(student);
    }

    public Boolean deleteStudent(Long id) {
        studentRepository.delete(id);
        return true;
    }
}
