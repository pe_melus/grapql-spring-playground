package com.seges.graphql.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.seges.graphql.model.Book;
import com.seges.graphql.model.Student;
import com.seges.graphql.repository.BookRepository;
import com.seges.graphql.repository.StudentRepository;

/**
 * Created by pmelus on 28.6.2018.
 */
@Component
public class Query implements GraphQLQueryResolver {

    private final StudentRepository studentRepository;
    private final BookRepository bookRepository;

    @Autowired
    public Query(StudentRepository studentRepository, BookRepository bookRepository) {
        this.studentRepository = studentRepository;
        this.bookRepository = bookRepository;
    }

    public Student getStudent(Long id) throws Exception {
        System.out.println("Get STUDENT by id: " + id);
        Student student;
        if ((student = studentRepository.findOne(id)) == null) {
            throw new Exception("zjebalo sa");
        }
        return student;
    }

    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    public Book getBook(Long id) {
        return bookRepository.findOne(id);
    }
}
