# grapql-spring-playground

Joy of learning alternate things

# Important things
* Project need schema file with suffix .graphqls 
    ```/resources/graphql/student.graphls``` 
* For creating endpoint for Query graphQL = implements GraphQLQueryResolver
* For Mutation graphQL =  implements GraphQLMutationResolver
* Every method defined in schema must be implemented also in code in Query or Mutation
